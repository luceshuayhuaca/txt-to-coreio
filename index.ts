import {District, Contest, Precinct, PrecinctDistrictMapping, Candidate, Language, Measure, BallotTypeContest, BallotTypeMeasure, Header} from './models';
import {CoreIO, Option, RotationSet, TranslatableText} from './coreio';
import { parseCsvFile } from "./PapaWrapper";
import { write } from './fileprocess';
const fs = require('fs');
const path = require('path');

const fileMap = {
  districts: 'DistrictExtract',
  candidates: 'CandidateExtract',
  contests: 'ContestExtract',
  measures: 'MeasureExtract',
  contestToStyleMapping: 'SequoiaContestBallotTypeXrefExtract',
  measureToStyleMapping: 'SequoiaMeasureBallotTypeXrefExtract',
  precinctToStyleMapping: 'SequoiaPctVotpctAvpctBTExtract',
  preacinctDistrictMapping: 'SequoiaPrecinctDistrictXrefExtract',
};

class Parser {

  coreio: CoreIO;

  constructor(private dir: string) {
    const allGood = this.runChecks();
    if (!allGood) {
      console.error('Failed validation.');
      return;
    }
  }

  async parse(accountId: string, electionId: string) {
    this.coreio = new CoreIO(accountId, electionId);

    const languages: Language[] = [
      {id: '1', name: 'English', code: 'en', isEnglish: true},
      {id: '2', name: 'Spanish', code: 'es', isEnglish: false},
      // @assumption - We don't have data for these
      {id: '3', name: 'Mandarin', code: 'zh-hant', isEnglish: false},
      {id: '4', name: 'Cantonese', code: 'cantonese', isEnglish: false},
      {id: '5', name: 'Taiwanese', code: 'taiwanese', isEnglish: false},
    ];
    languages.forEach(lang => this.coreio.addLanguage(lang.id, lang));

    const districts = await this.getDistricts();
    districts.forEach(district => {
      this.coreio.addDistrict(district.id, district);
    });

    const precincts = await this.getPrecincts();
    precincts.forEach(precinct => {
      this.coreio.addPrecinct(precinct.id, precinct);
    });

    const pctDisMapping = await this.getPrecinctDistrictMapping();
    pctDisMapping.forEach(mapping => {
      this.coreio.mapPrecinctToDistrict(mapping.precinctId, mapping.districtId);
    });

    //get contests
    const contests = await this.getContests();
    contests.forEach(contest => {
      this.coreio.addOffice(contest.id, {
        titles: contest.titleManager.getTextArray(languages),
        num_selections: contest.selections,
        num_writeins: contest.num_writeins,
        sequence: contest.sequence,
        // @assumption - It's ok to default the year to this year.
        term_start: new Date().getFullYear(),
        term: contest.termlength,
        external_district_ids: contest.external_district_ids,
      });
    })
    this.coreio.createBoxesFromOffices();

    const headerGroups = new Map<number, {}>();
    //get headers from contests
    let currentHeaderText = '';
    let headerIndex = 0;
    contests.sort((a, b) => a.sequence - b.sequence);
    contests.forEach(contest=>{
      const contestHeader = {id: '', name: contest.allHeaders, contestIds: []};
      if (contest.allHeaders !== currentHeaderText) {
        headerIndex++;
        currentHeaderText = contest.allHeaders;
        contestHeader.id = 'header-'+headerIndex;
        contestHeader.contestIds.push(contest.id);
        headerGroups.set(headerIndex, contestHeader)
        //@todo external_option_ids? translations?
        this.coreio.addBox(contestHeader.id,{
          type: 'header',
          titles: contest.headerManager.getTextArray(languages),
          sequence: contest.sequence - 1,
        });
      } else {
        const foundHeader = headerGroups.get(headerIndex);
        foundHeader['contestIds'].push(contest.id);
      }
    })

    const measures = await this.getMeasures();
    measures.forEach(measure => {
      this.coreio.addMeasure(measure.id, {
        titles: measure.titleManager.getTextArray(languages),
        text: measure.textManager.getTextArray(languages),
        num_selections: measure.selections,
        num_writeins: measure.num_writeins,
        sequence: measure.sequence,
        external_district_ids: measure.external_district_ids,
      });
      measure.options.forEach(option=>{
        try{
          const optionTitle = option.toLowerCase()
          this.coreio.addOption(optionTitle,{
            titles: [new TranslatableText(optionTitle)],
            type: 'question',
            sequence: measure.sequence
          })
        }catch(error){}
      })
    });
    this.coreio.createBoxesFromMeasures();

    //map yes or no to boxes
    measures.forEach(measure=>{
      measure.options.forEach(option=>{
        try{
          const title = option.toLowerCase();
          this.coreio.mapOptionToBox(title, measure.id);
        }catch(error){}
      })
    });
    //get headers from measures
    let currentHeader = '';
    let headerNum = headerGroups.size;
    measures.sort((a, b) => a.sequence - b.sequence);
    measures.forEach(measure=>{
      const measureHeader = {id: '', name: measure.allHeaders, contestIds: []};
      if (measure.allHeaders !== currentHeader) {
        headerNum++;
        currentHeader = measure.allHeaders;
        measureHeader.id = 'header-'+headerNum;
        measureHeader.contestIds.push(measure.id);
        headerGroups.set(headerNum, measureHeader)
        //@todo external_option_ids? translations?
        this.coreio.addBox(measureHeader.id,{
          type: 'header',
          titles: measure.headerManager.getTextArray(languages),
          sequence: measure.sequence - 1,
        });
      } else {
        const foundHeader = headerGroups.get(headerNum);
        foundHeader['contestIds'].push(measure.id);
      }
    })
    //add candidates and map them to options
    const candidates = await this.getCandidates();
    candidates.forEach(candidate => {
      this.coreio.addCandidate(candidate.id, {
        titles: candidate.titleManager.getTextArray(languages),
        sequence: candidate.sequence,
      });
    });
    this.coreio.createOptionsFromCandidates();
    //mapping candidates to boxes through contest id.
    candidates.forEach(candidate=>{
      try{
        this.coreio.mapOptionToBox(candidate.id, candidate.contestId);
      }catch(error){}
    });

    //get ballot styles
    let prevBallot = 0;
    precincts.sort((a, b) =>{
      return b.ballotType-a.ballotType});
    const ballotContests = await this.getBallotTypeContest();
    const BallotMeasures = await this.getBallotTypeMeasure();
    //@assumption:precincts has all ballot types.
    precincts.forEach(ballot => {
      try{
        if(ballot.ballotType!==prevBallot && !(isNaN(ballot.ballotType))){
          prevBallot=ballot.ballotType;
          const external_precinct_ids=[];
          precincts.forEach(p=>{
            if(ballot.ballotType==p.ballotType){
              external_precinct_ids.push((p.id).toString());
            }
          })
          const contest_measure_ids=[];
          ballotContests.forEach(bc=>{
            if(parseInt(bc.id )== ballot.ballotType){
              contest_measure_ids.push((bc.contest_id).toString());
            }
          });
          BallotMeasures.forEach(b=>{
            if(parseInt(b.id)==ballot.ballotType){
              contest_measure_ids.push((b.measure_id).toString());
            }
          })
          this.coreio.addStyle((ballot.ballotType).toString(), {
              name: (ballot.ballotType).toString(),
              code: (ballot.ballotType).toString(),
              external_box_ids: contest_measure_ids,
              external_precinct_ids
          });
        }
      } catch(error){}
    });

    // Add write-ins to boxes
    // @assumption - We're assuming that the num write-ins is the same as num selections
    const writeIns: Option[] = [];
    this.coreio.boxes.forEach(box => {
      if (box.type === 'contest') {
        for (let i = 0; i < box.num_selections; i++) {
          let writeIn = writeIns[i];
          if (writeIn === undefined) {
            writeIn = new Option({
              account_id: accountId,
              external_id: `writein-${i}`,
              external_election_id: '',
              external_ref_id: '',
              // @assumption - Write ins have no label. Is this ok?
              titles: [new TranslatableText('')],
              sequence: 9000 + i,
              type: 'writein',
            });
            writeIns.push(writeIn);
            this.coreio.addOption(writeIn.external_id, writeIn);
          }

          this.coreio.mapOptionToBox(writeIn.external_id, box.external_id);
        }
      }
    });
    //Add headers to all styles
    this.coreio.styles.forEach(style => {
      headerGroups.forEach(header => {
      const headerContestIds = header['contestIds'].map(val=>val.toString())
      // console.log(headerContestIds)
        if(style.external_box_ids.some(v=> headerContestIds.indexOf(v)>=0)){
          style.external_box_ids.push(header['id']);
        }
      })
    });

    // @tood: Create a rotation set for each ballot type and use the rotation value from contestToStyleMapping to rotate candidates
    const rotationSets = new Map();
    ballotContests.forEach(ballot=>{
      ballot.rotationNum
      let rotationSet = rotationSets.get(ballot.id);
       if (!rotationSet) {
        rotationSet = new RotationSet();
        rotationSet.id = ballot.id;
        rotationSet.external_precinct_ids = [];
        precincts.forEach(p=>{
          if(p.ballotType==ballot.id){
            rotationSet.external_precinct_ids.push(p.pid);
          }
        })
        rotationSets.set(ballot.id, rotationSet);
      }
      if (!rotationSet.external_boxes[ballot.contest_id]) {
        rotationSet.external_boxes[ballot.contest_id] = { options: {} };
      }
      const box = this.coreio.boxes.get(ballot.contest_id);
      if (box) {
        let options: Option[] = box.external_option_ids.map(id => {
          return this.coreio.options.get(id);
        });

        options = this.coreio.rotateOptions(options, ballot.rotationNum);

        options.forEach(option => {
          rotationSet.external_boxes[ballot.contest_id].options[option.external_id] = option.sequence;
        });
      }
    });
    rotationSets.forEach(set => {
      this.coreio.addRotationSet(set.id, set);
    });


  }

  async getBallotTypeContest(){
    const papaBallotContest= await this.callPapaAndNormalized(fileMap.contestToStyleMapping);
    return papaBallotContest.map(ballot=>{
      return new BallotTypeContest(ballot);
    })
  }
  async getBallotTypeMeasure(){
    const papaBallotMeasure= await this.callPapaAndNormalized(fileMap.measureToStyleMapping);
    return papaBallotMeasure.map(ballot=>{
      return new BallotTypeMeasure(ballot);
    })
  }

  async getContests(){
    const papaContests = await this.callPapaAndNormalized(fileMap.contests);

    const contests = new Map<string, Contest>();
    papaContests.forEach(data => {
      const contest = new Contest(data);

      // Checking for ID because we could get a blank row
      if (contest.id) {
        const existingContest = contests.get(contest.id);

        // The contest file includes multiple languages, so if the contest already exists we just want to translate
        if (existingContest) {
          existingContest.addTranslation(contest);
        } else {
          contests.set(contest.id, contest);
        }
      } else {
        console.warn('Contest id missing from data', data);
      }
    });

    return Array.from(contests.values());
  }

  async getDistricts(){
    const papaDistricts = await this.callPapaAndNormalized(fileMap.districts);
    return papaDistricts.map(district=>{
      return new District(district);
    })
  }
  async getPrecincts(){
    const papaPrecincts = await this.callPapaAndNormalized(fileMap.precinctToStyleMapping);
    return papaPrecincts.map(precinct => {
      return new Precinct(precinct);
    });
  }
  async getPrecinctDistrictMapping(){
    const papaPrecincts = await this.callPapaAndNormalized(fileMap.preacinctDistrictMapping);
    return papaPrecincts.map(data => {
      return new PrecinctDistrictMapping(data);
    });
  }

  async getCandidates(){
    const papaChoices = await this.callPapaAndNormalized(fileMap.candidates);

    const candidates = [];
    papaChoices.forEach(data => {
      const candidate = new Candidate(data);
      if (candidate.id) {
        candidates.push(candidate);
      } else {
        console.warn('Candidate id missing from data', data);
      }
    });

    return candidates;
  }

  async getMeasures(){
    const papaContests = await this.callPapaAndNormalized(fileMap.measures);

    const measures = [];
    papaContests.forEach(data => {
      const measure = new Measure(data);
      if (measure.id) {
        measures.push(measure);
      } else {
        console.warn('Measure id missing from data', data);
      }
    });

    return measures;
  }

  async callPapaAndNormalized(fileName): Promise<any[]> {
    const results = await parseCsvFile(this.dir+'/'+fileName);
    return results.data.map(row=>{
      const ret= {};
      for (let key in row) {
        const normalized = key.toLowerCase().replace(/[^a-z0-9]/g, '');
        ret[normalized] = row[key]
      }
      return ret;
    });
  }

  runChecks(){
    let files = [];
    fs.readdirSync(this.dir).forEach(file => {
        if(path.extname(file).toLowerCase() == '.txt'){
          files.push(file);
        } else{
          console.warn('invalid file format', file);
        }
    });

    for (let key in fileMap) {
      const fileName = files.find(name => name.includes(fileMap[key]));
      if (fileName) {
        fileMap[key] = fileName;
      } else {
        console.error('Missing required file: ' + fileMap[key]);
        return false;
      }
    }

    return true;
  }

  async parseAndSave(accountId: string, electionId: string): Promise<void> {
      const parseResults = await this.parse(accountId, electionId);
      const serializedData = JSON.stringify(this.coreio.getData(), null, ' ');
      await write('./output/more.json', serializedData);
      console.log("Done Writing to JSON file");
  }

}

let p = new Parser('./input');
// p.parse('dev', '1');
p.parseAndSave('dev', '1');
