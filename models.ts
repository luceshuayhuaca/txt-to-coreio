export class Header{
  id: string;
  contestIds = new Set();
  constructor(data){
    this.contestIds.add(data.id);
  }
}
export class TranslatableTextManager {
  items: TranslatableText[] = [];

  add(i: number, text: string, langId: string, style: TranslatableStyle = 'default', format: TranslatableFormat = 'style') {
    if (this.items[i] === undefined) {
      this.items[i] = new TranslatableText();
      this.items[i].style = style;
      this.items[i].format = format;
    }
    this.items[i].add(text, langId);
  }

  getText(languages: Language[]): TranslatableText {
    return this.setLangCodes(this.items[0], languages);
  }

  getTextArray(languages: Language[]): TranslatableText[] {
    return this.items.filter(item => {
      return item.value != '' || Object.keys(item.translations).length > 0;
    }).map(item => {
      return this.setLangCodes(item, languages);
    });
  }

  // Convert language names to codes
  setLangCodes(text: TranslatableText, langs: Language[]): TranslatableText {
    langs.forEach(lang => {
      const langId = lang.id;
      if (text.translations[langId]) {
        // @assumption - English is always defined like this
        if (lang.isEnglish) {
          text.value = text.translations[langId];
        } else {
          text.translations[lang.code] = text.translations[langId];
        }
        delete text.translations[langId];
      }
    });
    return text;
  }
}

export type TranslatableStyle = 'default'|'title'|'subtitle';
export type TranslatableFormat = 'default'|'text'|'style'|'html';

export class TranslatableText {
  value = '';
  format: TranslatableFormat = 'style';
  style: TranslatableStyle = 'default';
  translations: { [key: string]: string } = {};

  constructor(value: string = '') {
    this.value = value;
  }
  add(text: string, lang: string) {
    this.translations[lang] = text;
  }
}

export interface Language {
  id: string;
  name: string;
  code: string;
  isEnglish: boolean;
}

export class District {
  id: string;
  name: string;
  type: string;

  constructor(data: DistrictData) {
    this.id = data.ldistricthndl;
    this.type = data.szdistricttypeofficialdesc;
    this.name = data.szelecdistrictname;
  }
}

export class Precinct {
  id: string;
  pname: string;
  pid: string;
  sname: string;
  sid: string;

  ballotType: number;

  external_district_ids: string[] = [];

  constructor(data: PrecinctData) {
    // @assumption: Not using lPctPorHndl
    this.id = data.lprecincthndl;
    this.pid = data.sprecinctid;
    this.pname = data.szprecinctname;
    this.sid = data.sprecinctportion;

    this.ballotType = Number(data.ibaltype);
  }
}

export class PrecinctDistrictMapping {
  precinctId: string;
  districtId: string;

  constructor(data: PrecinctDistrictMappingData) {
    // @assumption: Not using portion handle
    this.precinctId = data.lprecincthndl;
    this.districtId = data.lsubdistricthndl;
  }
}

export class Party {
  id: string;
  name: string;
  abbreviation: string;
  nonPartisan: boolean;

  titleManager: TranslatableTextManager;

  constructor(data: PartyData) {
    this.id = data.externalid;
    this.name = data.name;
    this.abbreviation = data.abbreviation;
    this.nonPartisan = data.independent === '1';

    this.titleManager = new TranslatableTextManager();
  }

  setDisplayData(data: PartyDisplayData[]) {
    data.filter(item => {
      return item.partyname === this.name // Match the party
        && item.purpose === 'Audio' // Audio is the field that holds translations
    }).forEach(item => {
      const tus = +item.numoftu;
      for (let i = 0; i < tus; i++) {
        this.titleManager.add(i, item[`tu${i+1}`], item.language);
      }

      // @todo: tus/rtf - Sometimes it's possible to have only rtf defined.
      // We can't/don't want to rely on rtf because it has special formatting.
      const appearance = item.appearance.toLowerCase();
      if (tus === 0 && appearance === 'rtf' && item.rtftext) {
        this.titleManager.add(0, item.rtftext, item.language);
      }
    });

    // @todo: English not included on display data for sample
    this.titleManager.add(0, this.name, '1');
  }
}

export class Contest {
  static sequence = 10;

  id: string;
  text: TranslatableText[];
  selections: number;
  num_writeins: number;
  sequence: number;
  external_district_ids: string[] = [];

  name: string;
  titleManager: TranslatableTextManager;

  headerManager: TranslatableTextManager;

  header1: string;
  header1Manager: TranslatableTextManager;

  header2: string;
  header2Manager: TranslatableTextManager;

  header3: string;
  header3Manager: TranslatableTextManager;

  district: string;
  districthndl: number;
  termlength: number;

  districtId: string;
  languageId: string;

  headerNames: string[] = [];

  allHeaders: string;

  constructor(data: ContestData) {
    this.id = data.icontestid;
    this.selections = +data.inumtovotefor;
    this.num_writeins = +data.iwriteins;
    this.sequence = Contest.sequence;
    this.name = data.szofficetitle;
    this.languageId = data.llanguagehndl;

    // Save headers so we can build them later
    this.header1 = data.szgrouphdg;
    this.header1Manager = new TranslatableTextManager();
    this.header1Manager.add(0, this.header1, '1', 'default');
    this.header2 = data.szballotheading;
    this.header2Manager = new TranslatableTextManager();
    this.header2Manager.add(0, this.header2, '1', 'default');
    this.header3 = data.szsubheading;
    this.header3Manager = new TranslatableTextManager();
    this.header3Manager.add(0, this.header3, '1', 'default');
    this.headerManager = new TranslatableTextManager();
    this.allHeaders = this.header1+'-'+this.header2+'-'+this.header3;
    this.headerManager.add(0, this.allHeaders, '1', 'default');


    this.titleManager = new TranslatableTextManager();

    this.titleManager.add(0, this.name, data.llanguagehndl);

    // We are mapping off subdistrict handle if defined
    if (data.lsubdistricthndl) {
      this.districtId = data.lsubdistricthndl;
    } else {
      this.districtId = data.ldistricthndl;
    }
    this.external_district_ids = [this.districtId];

    Contest.sequence += 10;
  }

  addTranslation(contest: Contest) {
    if (contest.name) {
      this.titleManager.add(0, contest.name, contest.languageId);
    }

    if (contest.header1) {
      this.header1Manager.add(0, contest.header1, contest.languageId);
    }

    if (contest.header2) {
      this.header2Manager.add(0, contest.header2, contest.languageId);
    }

    if (contest.header3) {
      this.header3Manager.add(0, contest.header3, contest.languageId);
    }
  }
}


export class Measure {
  static sequence = 9000;
  id: string;
  text: TranslatableText[];
  selections: number;
  num_writeins: number;
  sequence: number;
  external_district_ids: string[] = [];

  titleManager: TranslatableTextManager;
  textManager: TranslatableTextManager;
  headerManager: TranslatableTextManager;
  district: string;
  ballothead: string;
  termlength: number;
  grouphdg: string;
  subhead: string;
  allHeaders: string;

  type: 'contest'|'measure'|'text'|'header';
  options: string[]=[];
  name: string;
  districtId: string;

  headerNames: string[] = [];
  constructor(data: MeasureData) {
    this.id = data.imeasureid;
    this.selections = 1;
    this.num_writeins = 0;
    this.sequence = Measure.sequence;

    // Save headers so we can build them later
    this.grouphdg = data.szgrouphdg;
    this.ballothead = data.szballotheading;
    this.subhead = data.szsubheading;
    this.allHeaders = this.grouphdg+'-'+this.ballothead+'-'+this.subhead;
    this.headerManager = new TranslatableTextManager();
    this.headerManager.add(0, this.allHeaders, '1', 'default');
    this.type = 'measure';
    this.name = data.szmeasureabbr1+'. '+data.szmeasureabbr2;
    // @assumption - There's no real measure title or text in this data, so just adding these so we have something to reference
    this.titleManager = new TranslatableTextManager();
    // this.titleManager.add(0, this.name, '1', 'default');
    this.titleManager.add(0, data.szmeasureabbr1, '1','default');
    this.titleManager.add(1, data.szmeasureabbr2, '1', 'subtitle');

    this.options = [data.szresponsetitle1, data.szresponsetitle2];

    this.textManager = new TranslatableTextManager();
    if (data.szballottextpath1) {
      this.textManager.add(0, data.szballottextpath1, '1', 'default', 'html')
    }

    // We are mapping off subdistrict handle if defined
    if (data.lsubdistricthndl) {
      this.districtId = data.lsubdistricthndl;
    } else {
      this.districtId = data.ldistricthndl;
    }
    this.external_district_ids = [this.districtId];

    Measure.sequence += 10;
  }
}

export class Candidate {
  static sequence = 10;

  id: string;
  name: string;
  titleManager: TranslatableTextManager;
  sequence: number;
  contestId: string;
  partyCode: string;

  constructor(data: CandidateData) {
    // @assumption - CandidateID does not appear to be globally distinct
    if (data.icandidateid && data.icontestid) {
      this.id = data.icontestid + '-' + data.icandidateid;
    }

    this.name = data.szcandidateballotname;
    this.sequence = +data.ivotingposition;

    this.partyCode = data.spartyabbr;
    this.contestId = data.icontestid;

    this.titleManager = new TranslatableTextManager();
    this.titleManager.add(0, data.szcandidateballotname, data.llanguagehndl);
    this.titleManager.add(1, data.szballotdesignation, data.llanguagehndl);
  }
}


export class BallotType {
  id: string;
  name: string;
  code: string;
  external_box_ids: string[] = [];
  external_precinct_ids: string[] = [];

  constructor(data: BallotTypeData) {
    this.id = data.externalid ? data.externalid : data.id;
    this.code = data.abbreviation ? data.abbreviation : data.id;
    this.name = data.name;
  }
}

export class BallotTypeContestMapper {
  styles = new Map<string, Set<string>>();

  constructor(data: BallotTypeContestData[]) {
    data.forEach(item => {
      let contests = this.styles.get(item.ballottypename);
      if (!contests) {
        contests = new Set();
        this.styles.set(item.ballottypename, contests);
      }
      contests.add(item.contestname);
    });
  }

  get(styleName: string) {
    return this.styles.get(styleName);
  }
}

export class BallotTypePrecinctMapper {
  styles = new Map<string, Set<string>>();

  constructor(data: BallotTypePrecinctData[]) {
    data.forEach(item => {
      let precincts = this.styles.get(item.ballottypename);
      if (!precincts) {
        precincts = new Set();
        this.styles.set(item.ballottypename, precincts);
      }
      precincts.add(item.precinctid);
    });
  }
  get(styleName: string) {
    return this.styles.get(styleName);
  }
}

export class BallotTypeContest {
  id: string;
  name: string;
  code: string;
  contest_id: string;
  rotationNum: string;

  constructor(data: BallotContestData) {
    this.id = data.ibaltype;
    this.name = data.ibaltype;
    this.code = data.ibaltype;
    this.contest_id= data.icontestid;
    this.rotationNum = data.icalctstrotation
  }
}
interface BallotContestData {
  ibaltype: string;
  icontestid:string;
  icalctstrotation: string;
}
export class BallotTypeMeasure {
  id: string;
  name: string;
  code:string;
  measure_id: string;

  constructor(data: BallotMeasureData) {
    this.id = data.ibaltype;
    this.name = data.ibaltype;
    this.code = data.ibaltype;
    this.measure_id= String(data.imeasureid);
  }
}
interface BallotMeasureData {
  ibaltype: string;
  imeasureid:number;
}

interface BallotTypeData {
  id: string;
  name: string;
  externalid: string;
  abbreviation: string;
  pollingdistrictlist: string;
  egc1: string;
}
interface BallotTypeContestData {
  id: string;
  ballottypename: string;
  contestname: string;
  rotationindex: string;
}
interface BallotTypePrecinctData {
  id: string;
  ballottypename: string;
  precinctname: string;
  precinctid: string;
}
interface BallotTypeMeasureData{
  imeasureid: string;
  ibaltype: string;
}
interface ContestData {
  selectionabbr: string;
  lofficemasterhndl: string;
  icontestid: string;
  lpartyhndl: string;
  spartyabbr: string;
  szofficeabbr1: string;
  szofficeabbr2: string;
  ldistricthndl: string;
  sdistrictid: string;
  lsubdistricthndl: string;
  isubdistrict: string;
  szgrouphdg: string;
  szballotheading: string;
  szsubheading: string;
  szofficetitle: string;
  llanguagehndl: string;
  slanguageabbr: string;
  inumtovotefor: string;
  iwriteins: string;
  scandidatesequence: string;
}
interface MeasureData {
  selectionabbr: string;
  imeasureid: string;
  sdesignation: string;
  szmeasureabbr1: string;
  szmeasureabbr2: string;
  ldistricthndl: string;
  sdistrictid: string;
  lsubdistricthndl: string;
  isubdistrict: string;
  llanguagehndl: string;
  slanguageabbr: string;
  szresponsetitle1: string;
  szresponsetitle2: string;
  szgrouphdg: string;
  szballotheading: string;
  szsubheading: string;
  szballottextpath1: string;
  szballottextpath2: string;
}
interface CandidateData {
  selectionabbr: string;
  icontestid: string;
  icandidateid: string;
  szcandnamelast: string;
  szcandnamefirst: string;
  szcandnamemiddle: string;
  scandnamesuffix: string;
  szcandidateballotname: string;
  szcandidateabbr: string;
  llanguagehndl: string;
  slanguageabbr: string;
  szballotdesignation: string;
  lpartyhndl: string;
  spartyabbr: string;
  ivotingposition: string;
}
interface HeaderData {
  id: string;
  name: string;
  purpose: string;
  paperappearance: string;
  screenappearance: string;
}
interface ElectionData {
  id: string;
  name: string;
  purpose: string;
}
interface LanguageData {
  id: string;
  name: string;
  purpose: string;
}
interface PartyData {
  id: string;
  name: string;
  abbreviation: string;
  description: string;
  imagename: string;
  independent: string;
  externalid: string;
  line1: string;
}
interface PartyDisplayData {
  id: string;
  partyname: string;
  language: string;
  purpose: string;
  appearance: string;
  rtftext: string;
  politicalpartytemplate: string;
  numoftu: string;
  tu1: string;
}
interface DistrictData {
  selectionabbr: string;
  ldisttypehndl: string;
  szdistricttypeofficialdesc: string;
  ldistricthndl: string;
  lparentdisthndl: string;
  selecdistrictid: string;
  ielecsubdistrict: string;
  szelecdistrictname: string;
}
interface PrecinctData {
  selectionabbr: string;
  lpctporhndl: string;
  lprecincthndl: string;
  sprecinctid: string;
  sprecinctportion: string;
  szprecinctname: string;
  ibaltype: string;
  sabsenteeprecinctid: string;
  szabsenteeprecinctname: string;
  imailballotprecinct: string;
  svotingprecinctid: string;
  szvotingprecinctname: string;
}
interface PrecinctDistrictMappingData {
  selectionabbr: string;
  lprecincthndl: string;
  sprecinctid: string;
  lprecinctportionhndl: string;
  sprecinctportion: string;
  ldistricthndl: string;
  selecdistrictid: string;
  lsubdistricthndl: string;
  ielecsubdistrict: string;
}
